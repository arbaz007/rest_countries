import CountryCard from "./CountryCard";
import noData from "./assets/noData.svg";
import Filter from "./Filter";
import { useContext } from "react";
import ThemeContext from "./ThemeContext";

import CardSkeleton from "./CardSkeleton";
let Home = ({
  loader,
  country,
  input,
  setInput,
  region,
  subregion,
  sort,
  setSort,
  handleSort,
  searchBy,
  SetRegion,
  subRegion,
  setSubRegion,
  handleSubRegion,
  selectedSubregion,
  setSelectedSubregion,
  handleSearch,
}) => {
  let countries = handleSort(sort, country).filter((each) => {
    if (region === "" || region === each.region) {
      if (
        (searchBy === "country" &&
          (input === "" ||
            each.name.common
              .toLowerCase()
              .includes(input.toLocaleLowerCase()))) ||
        (searchBy === "capital" &&
          (input === "" ||
            (each.capital &&
              each.capital[0]
                .toLocaleLowerCase()
                .includes(input.toLocaleLowerCase()))))
      ) {
        if (subregion === "" || subregion === each.subregion) {
          return each;
        }
      }
    }
  });
  let { dark, toggleTheme } = useContext(ThemeContext);
  return (
    <>
      
      <Filter
        input={input}
        setInput={setInput}
        region={region}
        SetRegion={SetRegion}
        subRegion={subRegion}
        setSubRegion={setSubRegion}
        selectedSubregion={selectedSubregion}
        setSelectedSubregion={setSelectedSubregion}
        handleSubRegion={handleSubRegion}
        sort={sort}
        setSort={setSort}
        handleSearch={handleSearch}
      />
      {loader && <CardSkeleton loop={10} />}
      {countries.length === 0 ? (
        <div className="no-country">
          <img src={noData} alt="svg image..." />
        </div>
      ) : (
        <div className="card-deck">
          {countries.map((each, index) => {
            return (
              <CountryCard
                key={index}
                flag={each?.flags?.png}
                name={each?.name?.common}
                pop={each?.population}
                reg={each?.region}
                cap={each?.capital}
                id={each?.cca2}
              />
            );
          })}
        </div>
      )}
    </>
  );
};
export default Home;
