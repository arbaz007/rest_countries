import Skeleton from "react-loading-skeleton";
import ThemeContext from "./ThemeContext";
import { useContext } from "react";
import "react-loading-skeleton/dist/skeleton.css";
import { useNavigate } from "react-router-dom";



let CountryCard = ({ flag, name, pop, reg, cap, id }) => {
  let { dark, toggleTheme } = useContext(ThemeContext);
  let navigate = useNavigate()
  let handleNavigate = () => {
    navigate(`/country/${id}`)
  }
  return (
    <>
      <div className="country-card" onClick={handleNavigate}>
        <div className="card-image">
          <img src={flag} alt="" />
        </div>
        <div
          className={`card-body ${dark ? "card-body-dark" : "card-body-light"}`}
        >
          <h2 className={`name ${dark ? "name-dark" : "name-light"}`}>
            {name}
          </h2>
          <div className="population">
            <span
              className={`${dark ? "country-span-dark" : "country-span-light"}`}
            >
              Population{" "}
            </span>
            : {pop.toLocaleString()}
          </div>
          <div className="region">
            <span
              className={`${dark ? "country-span-dark" : "country-span-light"}`}
            >
              Region{" "}
            </span>
            : {reg}
          </div>
          <div className="capital">
            <span
              className={`${dark ? "country-span-dark" : "country-span-light"}`}
            >
              Capital{" "}
            </span>
            :{cap ? cap[0] : "no capital"}
          </div>
        </div>
      </div>
    </>
  );
};
export default CountryCard;
