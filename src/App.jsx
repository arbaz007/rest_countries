import { useContext, useEffect, useState } from "react";

import "./App.css";
import ThemeContext from "./ThemeContext";
import "react-loading-skeleton/dist/skeleton.css";
import Home from "./Home";
import { Route, Routes } from "react-router-dom";
import CountryCardDetails from "./CountryCardDetails";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
function App() {
  let [region, SetRegion] = useState("");
  let [input, setInput] = useState("");
  let [country, setCountry] = useState([]);
  let [error, setError] = useState(false);
  let [loader, setLoader] = useState(true);
  let [subRegion, setSubRegion] = useState({});
  let [sort, setSort] = useState("");
  let [selectedSubregion, setSelectedSubregion] = useState("");
  let [searchBy, setSearchBy] = useState("country");

  useEffect(() => {
    async function getCountry() {
      try {
        let data = await fetch("https://restcountries.com/v3.1/all");
        if (!data) {
          throw new Error("Failed to fetch data");
        }
        data = await data.json();
        setCountry(data);
        setLoader(false);
      } catch (error) {
        setError(true);
      }
    }
    getCountry();
  }, []);
  let { dark, toggleTheme } = useContext(ThemeContext);
  let handleSubRegion = (region) => {
    let subReg = country.reduce((acc, curr) => {
      if (curr["region"] == region) {
        if (!acc[region]) {
          acc[region] = [];
          acc[region].push(curr["subregion"]);
        } else {
          if (!acc[region].includes(curr["subregion"])) {
            acc[region].push(curr["subregion"]);
          }
        }
      }
      return acc;
    }, {});
    setSubRegion(subReg);
  };
  let handleSearch = (value) => {
    setSearchBy(value);
    console.log(value);
  };

  let handleSort = (value, array) => {
    let ans = array;
    if (value === "ascending") {
      ans = array.toSorted((a, b) => {
        return a.population - b.population;
      });
    }
    if (value === "descending") {
      ans = array.toSorted((a, b) => {
        return b.population - a.population;
      });
    }
    if (value === "area-asc") {
      ans = array.toSorted((a, b) => {
        return a.area - b.area;
      });
    }
    if (value === "area-desc") {
      ans = array.toSorted((a, b) => {
        return b.area - a.area;
      });
    }
    return ans;
  };

  return (
    <div className={dark ? "body-dark" : "body"}>
      <nav className={`navbar ${dark ? "navbar-dark" : "navbar-light"}`}>
        <h1 className={`nav-header ${dark ? "nav-header-dark" : "nav-header"}`}>
          Where in the world?
        </h1>

        <button
          onClick={toggleTheme}
          className={`mode ${dark ? "mode-dark" : "mode-light"}`}
        >
          <FontAwesomeIcon icon={faMoon} style={{ marginRight: "1rem" }} />
          Dark Mode
        </button>
      </nav>
      <Routes>
        <Route
          path={"/"}
          element={
            <Home
              loader={loader}
              country={country}
              input={input}
              setInput={setInput}
              region={region}
              subregion={selectedSubregion}
              sort={sort}
              setSort={setSort}
              handleSort={handleSort}
              searchBy={searchBy}
              SetRegion={SetRegion}
              subRegion={subRegion}
              setSubRegion={setSubRegion}
              handleSubRegion={handleSubRegion}
              selectedSubregion={selectedSubregion}
              setSelectedSubregion={setSelectedSubregion}
              handleSearch={handleSearch}
            />
          }
        />
        <Route path="/country/:id" element={<CountryCardDetails />} />
      </Routes>
    </div>
  );
}

export default App;
