import { useContext } from "react";
import ThemeContext from "./ThemeContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
let Filter = ({
  input,
  setInput,
  region,
  SetRegion,
  subRegion,
  setSubRegion,
  selectedSubregion,
  setSort,
  setSelectedSubregion,
  handleSubRegion,
  handleSearch
}) => {
  let { dark, toggleTheme } = useContext(ThemeContext);
  return (
    <>
      <div className="inputs">
        <div
          className={`input-search ${
            dark ? "input-search-dark" : "input-search-light"
          }`}
        >
          <FontAwesomeIcon icon={faMagnifyingGlass} className="search-icon" />
          <input
            type="text"
            placeholder="Search for a country..."
            onChange={(e) => setInput(e.target.value)}
            value={input}
            style={dark ? {color: "hsl(0, 0%, 98%)"} : {color: 'hsl(0, 0%, 52%)'}}
          />
        </div>
        <div className="searh-select">
          <select onChange={e => handleSearch(e.target.value)} >
            <option value="country">search by country</option>
            <option value={"capital"}>search by capital</option>
          </select>
        </div>
        <div
          className={`input-select ${
            dark ? "input-select-dark" : "input-select-light"
          }`}
        >
          <select
            name="region"
            id="region"
            onChange={(e) => {
              SetRegion(e.target.value);
              e.target.value === ""
                ? setSelectedSubregion("")
                : handleSubRegion(e.target.value);
            }}
            className={`${dark ? "select-region-dark" : "select-region-light"}`}
          >
            <option value="">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
        <div
          className={`subregion-select ${
            dark ? "subregion-select-dark" : "subregion-select-light"
          }`}
        >
          <select
            name="subregion"
            id="subregion"
            onChange={(e) => setSelectedSubregion(e.target.value)}
            className={`${dark ? "select-region-dark" : "select-region-light"}`}
          >
            <option value={""} >Select Sub region</option>
            {subRegion[region] &&
              subRegion[region].map((each) => {
                return (
                  <>
                    <option value={each}>{each}</option>
                  </>
                );
              })}
          </select>
        </div>
        <div
          className={`sort-population-select ${
            dark
              ? "sort-population-select-dark"
              : "sort-population-select-light"
          }`}
        >
          <select
            name="sort-population"
            onChange={(e) =>
              setSort(e.target.value)
            }
            className={`${dark ? "sort-select-dark" : "sort-select-light"}`}
          >
            <option value={""}>Sort by</option>area-desc
            <option value={"ascending"}>population in Ascending</option>
            <option value={"descending"}>population in Descending</option>
            <option value={"area-asc"}>area in Ascending</option>
            <option value={"area-desc"}>area in Descending</option>
          </select>
        </div>
      </div>
    </>
  );
};
export default Filter;
